#!/bin/sh

set -eu


REQUIRED_COMMANDS="
	[
	command
	chown
	chmod
	cp
	dd
	echo
	exit
	getopts
	mount
	mkdir
	partprobe
	printf
	return
	sed
	sync
	test
	touch
	trap
	unlink
"


e_err()
{
	>&2 echo "ERROR: ${*}"
}

e_warn()
{
	>&2 echo "WARN: ${*}"
}

usage()
{
	echo "Usage: ${0} -d SD_CARD [OPTIONS]"
	echo
	echo "Setup an SD card for a raspberry"
	echo "    -c  'CRONTAB_ENTRY' to add to pi crontab, can either be a file or the content"
	echo "    -d  'SD_CARD' to format"
	echo "    -D  Dry run"
	echo "    -i  'UPDATE_FILE' to use for flashing"
	echo "    -h  Print this help text and exit"
	echo "    -H  'HOSTNAME' to set"
	echo "    -w  'WIFI' configuration to inject, can either be the file or the content"
	echo "    -x  'EXEC:LOC' to inject an executable inside the image at LOC"
	echo "        This option can be passed multiple times to add multiples entries"
	echo
	echo "SSH options"
	echo "    -a  'AUTHORIZED_KEYS' to accept, disable the password authentication when provided"
	echo "        Can either be the file or the content"
	echo "    -s  enable ssh service"
	echo "    -p  'PUBLIC_KEY' to inject"
	echo "    -k  'PRIVATE_KEY' to inject"
	echo "    -K  'KNOWN_HOSTS' to inject, can either be the file or the content"
	echo "    -P  'PROFILE' to inject in .ssh/profile, can either be the file or the content"

	echo "All options listed between [brackets] can also be passed in environment variables."
}
cleanup()
{
	if mountpoint --quiet -- "${mount_p1:-}"; then
		umount "${mount_p1}"
	fi
	if [ -f "${mount_p1:-}" ]; then
		unlink "${mount_p1}"
	fi

	if mountpoint --quiet -- "${mount_p2:-}"; then
		umount "${mount_p2}"
	fi

	if [ -f "${mount_p2:-}" ]; then
		unlink "${mount_p2}"
	fi

	${dry_run:+echo} sync

	trap EXIT
}

init()
{
	trap cleanup EXIT

	mount_p1="$(mktemp --directory --tmpdir="${TMPDIR:-/tmp}" "mount_p1.XXXXXX")"
	mount_p2="$(mktemp --directory --tmpdir="${TMPDIR:-/tmp}" "mount_p2.XXXXXX")"
}

flash_system()
{
	dd bs=4M if="${UPDATE_FILE?}" of="${target_device?}" conv=fsync
	partprobe "${target_device?}"
	sleep 1 # Needed to let the system discover both partitions
}

enable_ssh()
{
	if [ ! -b "${target_device}p1" ]; then
		e_err "Boot partition not found '${target_device}p1', impossible to enable ssh"
		exit 1
	fi
	mount "${target_device}p1" "${mount_p1}"
	touch "${mount_p1}"/ssh
	umount "${mount_p1}"
}

configure_ssh()
{
	if [ ! -b "${target_device}p2" ]; then
		e_err "Main partition not found '${target_device}p2', impossible to configure ssh"
		exit 1
	fi
	mount "${target_device}p2" "${mount_p2}"

	mkdir -p "${mount_p2}/home/pi/.ssh/"
	#Give all this information
	if [ -n "${PUBLIC_KEY}" ]; then
		cp "${PUBLIC_KEY}" "${mount_p2}/home/pi/.ssh/id_rsa.pub"
		if [ ! -f "${PRIVATE_KEY}" ]; then
			e_warn "Private key not found."
		else
			cp "${PUBLIC_KEY%.pub}" "${mount_p2}/home/pi/.ssh/id_rsa"
		fi
	fi

	if [ -n "${SSH_PROFILE}" ]; then
		if [ ! -f "${SSH_PROFILE}" ]; then
			echo "${SSH_PROFILE}" > "${mount_p2}/home/pi/.ssh/config"
		else
			cp "${SSH_PROFILE}" "${mount_p2}/home/pi/.ssh/config"
		fi
	fi

	if [ -n "${KNOWN_HOSTS}" ]; then
		if [ ! -f "${KNOWN_HOSTS}" ]; then
			echo "${KNOWN_HOSTS}" > "${mount_p2}/home/pi/.ssh/known_hosts"
		else
			cp "${KNOWN_HOSTS}" "${mount_p2}/home/pi/.ssh/known_hosts"
		fi
	fi

	if [ -n "${AUTHORIZED_KEYS}" ]; then
		if [ ! -f "${AUTHORIZED_KEYS}" ]; then
			echo "${AUTHORIZED_KEYS}" > "${mount_p2}/home/pi/.ssh/authorized_keys"
		else
			cp "${AUTHORIZED_KEYS}" "${mount_p2}/home/pi/.ssh/authorized_keys"
		fi
		sed --in-place "s@#PasswordAuthentication yes@PasswordAuthentication no@" "${mount_p2}/etc/ssh/sshd_config"
	fi
	chown 1000:1000 --recursive "${mount_p2}/home/pi/.ssh/"
	umount "${mount_p2}"
}

enable_wifi()
{
	if [ ! -b "${target_device}p1" ]; then
		e_err "Boot partition not found '${target_device}p1', impossible to enable wifi"
		exit 1
	fi
	mount "${target_device}p1" "${mount_p1}"
	#todo Helper to generate the conf, QA maybe ?
	if [ ! -f "${WIFI_CONF}" ]; then
		echo "${WIFI_CONF}" > "${mount_p1}/wpa_supplicant.conf"
	else
		cp "${WIFI_CONF}" "${mount_p1}/wpa_supplicant.conf"
	fi
	umount "${mount_p1}"
}

change_hostname()
{
	_hostname="${1?Missing argument to function}"
	if [ ! -b "${target_device}p2" ]; then
		e_err "Main partition not found '${target_device}p2', impossible to change hostname"
		exit 1
	fi
	mount "${target_device}p2" "${mount_p2}"
	echo "${_hostname}" > "${mount_p2}/etc/hostname"
	sed --in-place "s@raspberrypi@${_hostname}@" "${mount_p2}/etc/hosts"
	umount "${mount_p2}"
}

inject_script()
{
	_injected_script="${1?Missing argument to function}"
	_injected_script_location="${2?Missing argument to function}"
	if [ ! -b "${target_device}p2" ]; then
		e_err "Main partition not found '${target_device}p2', impossible to inject '${_injected_script}'"
		exit 1
	fi
	mount "${target_device}p2" "${mount_p2}"
	cp "${_injected_script}" "${mount_p2}/${_injected_script_location}"
	chmod +rx "${mount_p2}/${_injected_script_location}"
	umount "${mount_p2}"
}

add_crontab_entry()
{
	_crontab_entry="${1?Missing argument to function}"
	if [ ! -b "${target_device}p2" ]; then
		e_err "Main partition not found '${target_device}p2', impossible to update crontab"
		exit 1
	fi
	mount "${target_device}p2" "${mount_p2}"

	if [ ! -f "${_crontab_entry}" ]; then
		echo "${_crontab_entry}" >> "${mount_p2}/var/spool/cron/crontabs/pi"
	else
		cat "${_crontab_entry}" >> "${mount_p2}/var/spool/cron/crontabs/pi"
	fi
	chown 1000:1000 --recursive "${mount_p2}/var/spool/cron/crontabs/pi"
	chmod 600 "${mount_p2}/var/spool/cron/crontabs/pi"
	umount "${mount_p2}"
}

check_requirements()
{
	for _cmd in ${REQUIRED_COMMANDS}; do
		if ! _test_result="$(command -V "${_cmd}")"; then
			_test_result_fail="${_test_result_fail:-}${_test_result}\n"
		else
			_test_result_pass="${_test_result_pass:-}${_test_result}\n"
		fi
	done

	if [ -n "${_test_result_fail:-}" ]; then
		e_err "Self-test failed, missing dependencies."
		echo "======================================="
		echo "Passed tests:"
		# shellcheck disable=SC2059  # Interpret \n from variable
		printf "${_test_result_pass:-none\n}"
		echo "---------------------------------------"
		echo "Failed tests:"
		# shellcheck disable=SC2059  # Interpret \n from variable
		printf "${_test_result_fail:-none\n}"
		echo "======================================="
		exit 1
	fi
}

main()
{
	while getopts ":a:c:d:DhH:i:k:K:p:P:sw:x:" options; do
		case "${options}" in
		a)
			AUTHORIZED_KEYS="${OPTARG}"
			;;
		c)
			CRONTAB_ENTRY="${OPTARG}"
			;;
		d)
			target_device="${OPTARG}"
			;;
		D)
			dry_run="true"
			;;
		h)
			usage
			exit 0
			;;
		H)
			HOSTNAME="${OPTARG}"
			;;
		i)
			UPDATE_FILE="${OPTARG}"
			;;
		k)
			PRIVATE_KEY="${OPTARG}"
			;;
		K)
			KNOWN_HOSTS="${OPTARG}"
			;;
		p)
			PUBLIC_KEY="${OPTARG}"
			;;
		P)
			SSH_PROFILE="${OPTARG}"
			;;
		s)
			SSH_ENABLED="true"
			;;
		w)
			WIFI_CONF="${OPTARG}"
			;;
		x)
			SCRIPT_LOC="${SCRIPT_LOC:-} ${OPTARG}"
			;;
		:)
			e_err "Option -${OPTARG} requires an argument."
			exit 1
			;;
		?)
			e_err "Invalid option: -${OPTARG}"
			exit 1
			;;
		esac
	done

	check_requirements
	init

	if [ -z "${dry_run:-}" ] && [ ! -b "${target_device:-}" ]; then
		e_err "${target_device:+SD_CARD} is not a block device, exiting"
		usage
		exit 1
	fi
	if [ -n "${UPDATE_FILE:-}" ]; then
		if [ ! -f "${UPDATE_FILE:-}" ]; then
			e_err "Installation image '${UPDATE_FILE:+UPDATE_FILE}' can't be found."
			usage
			exit 1
		fi
		${dry_run:+echo} flash_system
	fi

	if [ -n "${SSH_ENABLED:-}" ]; then
		${dry_run:+echo} enable_ssh
		${dry_run:+echo} configure_ssh
	fi

	if [ -n "${WIFI_CONF:-}" ]; then
		if ${dry_run:+echo} enable_wifi "${WIFI_CONF}"; then
			echo "WIFI setup"
		fi
	fi

	if [ -n "${HOSTNAME:-}" ]; then
		if ${dry_run:+echo} change_hostname "${HOSTNAME}"; then
			echo "Hostname setup"
		fi
	fi

	for injection in ${SCRIPT_LOC}; do
		_script_to_inject="${injection%%:*}"
		_location_to_inject="${injection##*:}"
		if ${dry_run:+echo} inject_script "${_script_to_inject}" "${_location_to_inject}"; then
			echo "${_script_to_inject} injected at ${_location_to_inject}"
		fi
	done

	if [ -n "${CRONTAB_ENTRY:-}" ]; then
		if ${dry_run:+echo} add_crontab_entry "${CRONTAB_ENTRY}"; then
			echo "Crontab entry added"
		fi
	fi

	cleanup
}
main "${@}"
exit 0
